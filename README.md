This repository contains a simple CI forwarding job, that allows the
`python-aside/aside` project to publish its GitLab Pages under the "root"
[python-aside.gitlab.io](https://python-aside.gitlab.io) url.

The actual development for the `aside` python package happens in the
[gitlab.com/python-aside/aside](https://gitlab.com/python-aside/aside)
repository.
